/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.client.net;

import se.kth.id1212.fbirwe.sockets.hangman.client.view.InputState;

/**
 *
 * @author fredericbirwe
 */
public interface OutputHandler {
    public void handleMsg(String msg);
    
    public void setInputState(InputState inputState);
}
