/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.common;

import java.util.ArrayList;
import se.kth.id1212.fbirwe.sockets.hangman.server.model.HangmanGame;

/**
 *
 * @author fredericbirwe
 */
public class StatusMessage extends Message{
    
    private ArrayList<Character> correctGuessed;
    private String word;
    private int remainingAttempts;
    
    public StatusMessage(HangmanGame curGame) {
        super(MsgType.STATUS);
        this.correctGuessed = curGame.getCorrectGuessed();
        this.word = curGame.getWord();
        this.remainingAttempts = curGame.getRemainingAttempts();
        
        calcSize();        
    }
    
    public ArrayList<Character> getCorrectGuessed() {
        return this.correctGuessed;
    }
    
    public int getRemainingAttempts() {
        return this.remainingAttempts;
    }
    
    public String getWord() {
        return this.word;
    }
    
}
