/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.client.net;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.sockets.hangman.client.view.InputState;
import se.kth.id1212.fbirwe.sockets.hangman.client.view.Texts;

import se.kth.id1212.fbirwe.sockets.hangman.common.Constants;
import se.kth.id1212.fbirwe.sockets.hangman.common.GameEndMessage;
import se.kth.id1212.fbirwe.sockets.hangman.common.Helper;
import se.kth.id1212.fbirwe.sockets.hangman.common.Message;
import se.kth.id1212.fbirwe.sockets.hangman.common.MsgType;
import se.kth.id1212.fbirwe.sockets.hangman.common.StatusMessage;

/**
 *
 * @author fredericbirwe
 */
public class ClientNet {
    Socket client;
    private final static int PORT = Constants.PORT;
    private final static String IP = Constants.IP;
    
    private ObjectInputStream fromServer;
    private ObjectOutputStream toServer;
    private Scanner sc;
    private OutputHandler outputHandler;
    private Listener listener;
    
    public ClientNet(OutputHandler outputHandler) {
        this.outputHandler = outputHandler;
        
        try {
            client = new Socket( IP, PORT );

            toServer = new ObjectOutputStream(client.getOutputStream());
            fromServer = new ObjectInputStream(client.getInputStream());
            listener = new Listener();
            
            new Thread(listener).start();
            
        } catch (IOException ex) {
            
        }
        

    }   
    
    
    public void disconnect() {
        try {
            client.close();
        } catch (IOException ex) {
            Logger.getLogger(ClientNet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void sendMessage(Message msg) throws IOException {
        toServer.writeObject(msg);
        toServer.flush();
        toServer.reset();
    }
    
    private class Listener implements Runnable {
        @Override
        public void run() {
                outputHandler.handleMsg("Connection established to: " + IP + ":" + PORT );
                outputHandler.handleMsg( Texts.WELCOME );
                outputHandler.handleMsg( Texts.RULES );
                
                try {
                    while(true) {                   
                        Message incoming = (Message) fromServer.readObject();                        
                        
                        if(incoming.getSize() == Helper.sizeof(incoming)) {
                        
                            // Print received Message
                            if(incoming.getType() == MsgType.STATUS) {                                
                                outputHandler.handleMsg(Texts.showStatus((StatusMessage) incoming));

                            } else if (incoming.getType() == MsgType.GAME_WON || incoming.getType() == MsgType.GAME_LOST) {
                                Message restart;

                                outputHandler.setInputState(InputState.RETRY);
                                outputHandler.handleMsg(incoming.getType() == MsgType.GAME_WON ? Texts.GAME_WON : Texts.GAME_LOST);
                                outputHandler.handleMsg( Texts.showSolution( (GameEndMessage) incoming ) );
                                outputHandler.handleMsg(Texts.showScore((GameEndMessage) incoming));
                                outputHandler.handleMsg( Texts.TRY_AGAIN );
                            }
                        }
                    }
                } catch (Exception ex) {
                    Logger.getLogger(ClientNet.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    disconnect();
                }

        }
    }
}
