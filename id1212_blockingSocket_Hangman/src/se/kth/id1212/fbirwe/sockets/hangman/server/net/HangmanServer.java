/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.server.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

/**
 *
 * @author fredericbirwe
 */
public class HangmanServer {
    private static final int PORT = 8083;
    private static final int LINGER_TIME = 5000;
    private static final int TIMEOUT = 600000;
    private ArrayList<ClientHandler> players = new ArrayList<>();
    
    public void serve() {
        try {
            ServerSocket listeningSocket = new ServerSocket(PORT);
            System.out.println("Server listening on port " + PORT);
            while (true) {
                Socket clientSocket = listeningSocket.accept();
                startHandler(clientSocket);
            }
        } catch (IOException e) {
            System.err.println("Server failure.");
        }
    }
    
    private void startHandler(Socket clientSocket) throws SocketException {
        clientSocket.setSoLinger(true, LINGER_TIME);
        clientSocket.setSoTimeout(TIMEOUT);
        ClientHandler handler = new ClientHandler(this, clientSocket);
        synchronized (players) {
            players.add(handler);
        }
        Thread handlerThread = new Thread(handler);
        handlerThread.setPriority(Thread.MAX_PRIORITY);
        handlerThread.start();
    }
    
    public void removeHandler(ClientHandler handler) {
        players.remove(handler);
    }

}
