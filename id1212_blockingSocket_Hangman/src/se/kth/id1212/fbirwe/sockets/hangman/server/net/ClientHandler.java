package se.kth.id1212.fbirwe.sockets.hangman.server.net;

import se.kth.id1212.fbirwe.sockets.hangman.common.WrongMessageSizeException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UncheckedIOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.sockets.hangman.common.GameEndMessage;
import se.kth.id1212.fbirwe.sockets.hangman.common.GuessCharMessage;
import se.kth.id1212.fbirwe.sockets.hangman.common.GuessWordMessage;
import se.kth.id1212.fbirwe.sockets.hangman.common.Helper;
import se.kth.id1212.fbirwe.sockets.hangman.common.Message;
import se.kth.id1212.fbirwe.sockets.hangman.common.MsgType;
import se.kth.id1212.fbirwe.sockets.hangman.common.StatusMessage;
import se.kth.id1212.fbirwe.sockets.hangman.server.controller.HangmanServerController;

/**
 *
 * @author fredericbirwe
 */
public class ClientHandler implements Runnable {
    private HangmanServer server;
    private Socket playerSocket;    
    private ObjectInputStream fromClient;
    private ObjectOutputStream toClient;
    
    public ClientHandler(HangmanServer server, Socket playerSocket) {
        this.server = server;
        this.playerSocket = playerSocket;
    }

    @Override
    public void run() {
        try {
            fromClient = new ObjectInputStream(playerSocket.getInputStream());
            toClient = new ObjectOutputStream(playerSocket.getOutputStream());
            HangmanServerController controller = new HangmanServerController();
            controller.start();
  
            Message msg;
            try {
                while((msg = ( (Message) fromClient.readObject() ) ) != null) {
                    
                    System.out.println(msg.getType() + " | " + msg.getSize() + " – " + Helper.sizeof(msg));
                    
                    if(msg.getSize() == Helper.sizeof(msg)) {
                        if(msg.getType() == MsgType.DISCONNECT) {
                            endSession();
                        }

                        if(msg.getType() == MsgType.GUESS_CHAR) {
                            controller.guess(((GuessCharMessage) msg).getChar());
                        }

                        if(msg.getType() == MsgType.GUESS_WORD) {
                            controller.guess(((GuessWordMessage) msg).getWord());
                        }

                        if(msg.getType() == MsgType.RESET) {
                            controller.restart();
                        }


                        if(msg.getType() != MsgType.RESET) {
                            Message out;

                            if(controller.hasWon()) {
                                out = new GameEndMessage(true, controller.getScore(), controller.getWord());
                            } else if (controller.hasLost()) {
                                out = new GameEndMessage(false, controller.getScore(), controller.getWord());
                            } else {
                                out = new StatusMessage(controller.getCurGame());                        
                            }

                            sendMessage(out);

                        }
                    }
                    
                    
                }            
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    private void sendMessage(Message out) throws IOException {
        toClient.writeObject(out);
        toClient.flush();
        toClient.reset();  
    }
    
    private void endSession() {
        System.out.println("Socket closed");
        try {
            this.playerSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        server.removeHandler(this);
        
    }
    
}
