/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.client.controller;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.sockets.hangman.client.net.ClientNet;
import se.kth.id1212.fbirwe.sockets.hangman.client.net.OutputHandler;
import se.kth.id1212.fbirwe.sockets.hangman.client.view.InputState;
import se.kth.id1212.fbirwe.sockets.hangman.client.view.Texts;
import se.kth.id1212.fbirwe.sockets.hangman.common.DisconnectMessage;
import se.kth.id1212.fbirwe.sockets.hangman.common.GuessCharMessage;
import se.kth.id1212.fbirwe.sockets.hangman.common.GuessWordMessage;
import se.kth.id1212.fbirwe.sockets.hangman.common.Message;
import se.kth.id1212.fbirwe.sockets.hangman.common.ResetMessage;

/**
 *
 * @author fredericbirwe
 */
public class ClientController {
    OutputHandler outputHandler;
    ClientNet net;
    
    public ClientController(OutputHandler outputHandler) {
        this.outputHandler = outputHandler;
        this.net = new ClientNet(outputHandler);
    }
    
    public void guess(String s) throws IOException {
        sendMessage( new GuessWordMessage(s) );
    }
    
    public void guess(char c) throws IOException {
        sendMessage( new GuessCharMessage(c) );
    }
    
    public void retry(boolean newGame) throws IOException {
        this.outputHandler.setInputState(InputState.GUESS);
        
        if ( newGame ) {
            sendMessage( new ResetMessage() );
        } else {
            sendMessage( new DisconnectMessage() );
        }
    }
    
    public void showRules() {
        this.outputHandler.handleMsg( Texts.RULES );
    }
    
    private void sendMessage( Message msg ) throws IOException {        
        CompletableFuture.runAsync(
            () -> {
                try {
                    this.net.sendMessage(msg);
                } catch (IOException ex) {
                    Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        );
    }
}
