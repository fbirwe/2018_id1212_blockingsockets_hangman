/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.client.view;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.kth.id1212.fbirwe.sockets.hangman.client.controller.ClientController;
import se.kth.id1212.fbirwe.sockets.hangman.client.net.OutputHandler;

/**
 *
 * @author fredericbirwe
 */
public class HangmanView implements Runnable {
    private static final String PROMPT = "> ";
    
    ClientController controller;
    Scanner sc;
    volatile InputState inputState;
    

    public HangmanView() {
        this.controller = new ClientController( new ConsoleOutput() );
        this.sc = new Scanner(System.in);
        inputState = InputState.GUESS;
        
        this.start();
    }
    
    public void start() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        while(true) {
            try {
                String next = getInput();

                if(inputState == InputState.RETRY) {
                    boolean retry = getYesOrNo(next);
                    controller.retry(retry);
                } else {
                    if(next.trim().toLowerCase().equals("rules")) {
                        controller.showRules();
                    } else {
                        if(next.length() == 1) {
                            controller.guess(next.charAt(0));
                        } else {
                            controller.guess(next);
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(HangmanView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private String getInput() {
        safePrint(PROMPT);
        return sc.nextLine();
    }
    
    /**
     * The method receives an input from the user and brings it down to yes or no.
     * Whitespace in the beginning or the end of the input, lower- and uppercase and interpunction are ignored.
     * Valid inputs for yes are "yes" and "y", everything else is interpreted as "no"
     * @return yes or no
     */
    private boolean getYesOrNo() {
        String input = getInput().trim().toLowerCase().replaceAll("\\W", "");
        
        return (input.equals("yes") || input.equals("y"));
    }
    
    private boolean getYesOrNo(String in) {
        String input = in.trim().toLowerCase().replaceAll("\\W", "");
        
        return (input.equals("yes") || input.equals("y"));
    }
    
    private synchronized void resetInputState( InputState inputState ) {
        this.inputState = inputState;
    }
    
    private synchronized void safePrint(String s) {
        System.out.print(s);
    }
    
    private synchronized void safePrintln(String s) {
        System.out.println(s);
    }
    
    private class ConsoleOutput implements OutputHandler {

        @Override
        public void handleMsg(String msg) {
            safePrintln(msg);
        }

        @Override
        public void setInputState(InputState inputState) {
            resetInputState( inputState );
        }
    
    }
}
