/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class GameEndMessage extends Message {
    private int score;
    private String word;
    
    public GameEndMessage(boolean isWon, int score, String word) {
        super(isWon ? MsgType.GAME_WON : MsgType.GAME_LOST);
        this.score = score;
        this.word = word;
        
        calcSize();
        
    }
    
    public int getScore() {
        return this.score;
    }
    
    public String getWord() {
        return this.word;
    }
    
}