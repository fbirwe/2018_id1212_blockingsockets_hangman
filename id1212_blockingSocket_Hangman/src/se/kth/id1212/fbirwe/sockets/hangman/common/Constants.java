/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class Constants {
    public static final int PORT = 8083; 
    public static final String IP = "localhost";
    public static final String DICTIONARY = "words";
}
