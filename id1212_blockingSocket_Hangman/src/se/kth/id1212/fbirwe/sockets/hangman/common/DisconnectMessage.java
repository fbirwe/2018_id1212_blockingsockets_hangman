/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.common;

/**
 *
 * @author fredericbirwe
 */
public class DisconnectMessage extends Message{
    
    public DisconnectMessage() {
        super(MsgType.DISCONNECT);
        
        calcSize();
    }
    
}
