/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.server.controller;

import se.kth.id1212.fbirwe.sockets.hangman.server.model.HangmanGame;
import se.kth.id1212.fbirwe.sockets.hangman.server.model.HangmanPlayer;

/**
 *
 * @author fredericbirwe
 */
public class HangmanServerController {
    private HangmanPlayer player;
    
    public HangmanServerController() {
        player = new HangmanPlayer();
    }

    public HangmanGame getCurGame() {
        return player.getCurGame();
    }
    
    public int getScore() {
        return player.getScore();
    }
    
    public String getWord() {
        return player.getWord();
    }
    
    public void guess(char c) {
        player.guess(c);
    }
    
    public void guess(String s) {
        player.guess(s);
    }

    public boolean  hasLost() {
        return player.hasLost();
    }
    
    public boolean hasWon() {
        return player.hasWon();
    }
  
    public void restart() {
        player.restart();
    }
    
    public void start() {
        player.start();
    }

}
