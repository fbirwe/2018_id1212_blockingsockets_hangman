/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.kth.id1212.fbirwe.sockets.hangman.common;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fredericbirwe
 */
public class Message implements Serializable {
    private MsgType type;
    private int size;
    
    public Message(MsgType type) {
        this.type = type;
        this.size = 0;
    }
    
    public MsgType getType() {
        return this.type;
    }
    
    public int getSize() {
        return this.size;
    }
    
    protected void calcSize() {
        try {
            this.size = Helper.sizeof(this);
        } catch (IOException ex) {
            Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
